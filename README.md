# smloader

SMLoader is a modloader for the ShellMMO game

### Mods

found them in 'moddb' release

### Installing

execute 'DESTDIR=/path/to/game ./install.sh'

### Loading

insert mod .tar files to the mods/ directory. Remember to not delete any files from the coremods/ directory. There mods add some features like the Mods button in the menu.

### Creating mods

Create a (mod name) directory, with a master.xml file inside:

	Name: mod-name
	Version: mod-version
	Master: mods-creator
	Description: the-desc

Next, mkdir p (patches) and r (replaces) directories.

In the 'p' directory place patches (.patch files) with base files' modification.

If you want to add new files, place them entire to the 'r' directory.

Next, just create a tar with command like this 'tar cvf hack-1.0-master.tar hack/'.
