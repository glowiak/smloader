#!/bin/sh
DESTDIR=${DESTDIR:-/tttmmmpp}
CWD=$(pwd)
PRODUCT="SMLoader"
if [ ! -f "$DESTDIR/shellmmo.sh" ]
then
	echo "Please export the correct \$DESTDIR variable with your SM path"
	exit 1
fi
echo "Installing $PRODUCT to $DESTDIR..."
mkdir -p $DESTDIR/mods
cp -rfv $CWD/coremods $DESTDIR/coremods
install -Dm775 $CWD/src/smloader.sh $DESTDIR/smloader
