#!/bin/sh
CWD=$(pwd)
MEX=$CWD/shellmmo.sh
SELFF=SMLoader

if [ ! -f "$MEX" ]
then
	echo "Install $SELFF in correct place!"
	exit 1
fi

clear
echo "Starting the $SELFF toolchain..."
MODLIST="$(ls $CWD/mods/)"
echo "Mods: $MODLIST"
echo "Live-copying game..."
rm -rf /tmp/smlgame
cp -rfv $CWD/ /tmp/smlgame
mkdir -p /tmp/smlgame/modsdir
echo "Extracting modfiles..."
for a in $(ls /tmp/smlgame/mods/)
do
	tar xvf /tmp/smlgame/mods/$a -C /tmp/smlgame/modsdir
done
for b in $(ls /tmp/smlgame/coremods/)
do
	tar xvf /tmp/smlgame/coremods/$b -C /tmp/smlgame/modsdir
done
echo "Loading..."
for c in $(ls /tmp/smlgame/modsdir/)
do
	echo "Loading mod $c..."
	cd /tmp/smlgame
	echo "Patching..."
	for patchfile in $(ls /tmp/smlgame/modsdir/$c/p/)
	do
		patch -Np1 -i /tmp/smlgame/modsdir/$c/p/$patchfile
	done
	echo "Replacing..."
	cd /tmp/smlgame/modsdir/$c/r/
	rm -f /tmp/mod1.tar
	tar cf /tmp/mod1.tar .
	cd /tmp/smlgame
	tar xvf /tmp/mod1.tar
	rm -f /tmp/mod1.tar
done
echo "Mods loaded. Starting game..."
cd /tmp/smlgame
rm -rf /tmp/smlgame/game/saves
ln -sv $CWD/game/saves /tmp/smlgame/game/saves
sh shellmmo.sh
rm -rf /tmp/smlgame
